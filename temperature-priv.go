package temperature

const kelvinDelta = 273.15

func converter(iScale, oScale string, iTemp float64) (success bool, err, unit string, temp float64) {
	success = false
	switch iScale {
	case "Celcius",
		"Centigrade",
		"celcius",
		"centigrade",
		"C",
		"c":
		//Can add more permitations as necessary
		success, err, unit, temp = celcius(oScale, iTemp)
		break
	case "Farenheit",
		"farenheit",
		"F",
		"f":
		//Can add more permitations as necessary
		success, err, unit, temp = farenheit(oScale, iTemp)
		break
	case "Kelvin",
		"kelvin",
		"K",
		"k":
		//Can add more permitations as necessary
		success, err, unit, temp = kelvin(oScale, iTemp)
		break
	default:
		//Not a known scale
		err = iScale + " is not a known temperature scale, see help for more information"
	}
	return success, err, unit, temp
}

func celcius(oScale string, iTemp float64) (success bool, err, unit string, temp float64) {
	success = true
	switch oScale {
	case "Farenheit",
		"farenheit",
		"F",
		"f":
		//Can add more permitations as necessary
		unit = "°F"
		temp = cToF(iTemp)
		break
	case "Kelvin",
		"kelvin",
		"K",
		"k":
		//Can add more permitations as necessary
		unit = "°K"
		temp = cToK(iTemp)
		break
	default:
		//Not a known scale
		success = false
		err = oScale + " is not a known temperature scale, see help for more information"
	}
	return success, err, unit, temp
}

func farenheit(oScale string, iTemp float64) (success bool, err, unit string, temp float64) {
	success = true
	switch oScale {
	case "Celcius",
		"Centigrade",
		"celcius",
		"centigrade",
		"C",
		"c":
		//Can add more permitations as necessary
		unit = "°C"
		temp = fToC(iTemp)
		break
	case "Kelvin",
		"kelvin",
		"K",
		"k":
		//Can add more permitations as necessary
		unit = "°K"
		temp = fToK(iTemp)
		break
	default:
		//Not a known scale
		success = false
		err = oScale + " is not a known temperature scale, see help for more information"
	}
	return success, err, unit, temp
}

func kelvin(oScale string, iTemp float64) (success bool, err, unit string, temp float64) {
	success = true
	switch oScale {
	case "Celcius",
		"Centigrade",
		"celcius",
		"centigrade",
		"C",
		"c":
		//Can add more permitations as necessary
		unit = "°C"
		temp = kToC(iTemp)
		break
	case "Farenheit",
		"farenheit",
		"F",
		"f":
		//Can add more permitations as necessary
		unit = "°F"
		temp = kToF(iTemp)
		break
	default:
		//Not a known scale
		success = false
		err = oScale + " is not a known temperature scale, see help for more information"
	}
	return success, err, unit, temp
}

func fToC(f float64) float64 {
	return (f*5 - 160) / 9
}

func cToF(c float64) float64 {
	return (c*9 + 160) / 5
}

func fToK(f float64) float64 {
	return (fToC(f)) + kelvinDelta
}

func cToK(c float64) float64 {
	return c + kelvinDelta
}

func kToC(k float64) float64 {
	return k - kelvinDelta
}

func kToF(k float64) float64 {
	return cToF(k - kelvinDelta)
}