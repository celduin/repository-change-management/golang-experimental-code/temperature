# Temperature Conversion Library

A simple golang library package to convert one temperature scale to another.  The supported scales are:

- Celcius
- Farenheit
- Kelvin

There are two public functions for the package:

1. Start(): No inputs, No Outputs: Invokes the library, causing prompts for inputs to perform the conversion.
2. Help(): No inputs, No outputs: Triggers printing of help text for the package.