package temperature

import "fmt"

/*
Start function is used to get the required data and instigate the conversion
*/
func Start() {
	var (
		startScale string
		endScale   string
		unit       string
		inputTemp  float64
		outputTemp float64
		success    bool
		err        string
	)

	fmt.Println("What temperature scale are you entering?")
	fmt.Scanln(&startScale)
	fmt.Println("What temperature scale are you converting to?")
	fmt.Scanln(&endScale)
	fmt.Println("What is the input temperature?")
	fmt.Scanln(&inputTemp)

	success, err, unit, outputTemp = converter(startScale, endScale, inputTemp)
	if success {
		fmt.Printf("%.2f %s equals %.2f%s\n", inputTemp, startScale, outputTemp, unit)
	} else {
		fmt.Println(err)
	}
}

/*
Help function prints help text for the temperature converter tool
*/
func Help() {
	fmt.Println("==========Temperature Conversion Tool===========")
	fmt.Println("You will be prompted for the following:")
	fmt.Println("- Input temperature scale")
	fmt.Println("- Output temperature scale")
	fmt.Println("- Input temperature value")
	fmt.Println("The resultant conversion will be then outputted")
}
